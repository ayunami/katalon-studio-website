$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/01_register.feature");
formatter.feature({
  "name": "register",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "TC.Reg.001.001 - user can register with valid information",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is in register page",
  "keyword": "Given "
});
formatter.match({
  "location": "register.user_is_in_register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid information and click register button",
  "keyword": "When "
});
formatter.match({
  "location": "register.user_input_valid_information_and_click_register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can successfully register with valid information",
  "keyword": "Then "
});
formatter.match({
  "location": "register.user_can_successfully_register_with_valid_information()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "user cannot register with wrong email format and registered email",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is in register page",
  "keyword": "Given "
});
formatter.step({
  "name": "user input \u003ccondition\u003e in register page",
  "keyword": "When "
});
formatter.step({
  "name": "user click register button",
  "keyword": "And "
});
formatter.step({
  "name": "user get pop up \u003cresult\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "case_id",
        "condition",
        "result"
      ]
    },
    {
      "cells": [
        "TC.Reg.001.002",
        "wrong email format and valid password",
        "Please include an \u0027@\u0027 in the email address."
      ]
    },
    {
      "cells": [
        "TC.Reg.001.003",
        "registered email and valid password",
        "Email has already been taken"
      ]
    }
  ]
});
formatter.scenario({
  "name": "user cannot register with wrong email format and registered email",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is in register page",
  "keyword": "Given "
});
formatter.match({
  "location": "register.user_is_in_register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input wrong email format and valid password in register page",
  "keyword": "When "
});
formatter.match({
  "location": "register.user_input_in_register_page(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click register button",
  "keyword": "And "
});
formatter.match({
  "location": "register.user_click_register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user get pop up Please include an \u0027@\u0027 in the email address.",
  "keyword": "Then "
});
formatter.match({
  "location": "register.user_get_pop_up(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user cannot register with wrong email format and registered email",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is in register page",
  "keyword": "Given "
});
formatter.match({
  "location": "register.user_is_in_register_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input registered email and valid password in register page",
  "keyword": "When "
});
formatter.match({
  "location": "register.user_input_in_register_page(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click register button",
  "keyword": "And "
});
formatter.match({
  "location": "register.user_click_register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user get pop up Email has already been taken",
  "keyword": "Then "
});
formatter.match({
  "location": "register.user_get_pop_up(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/02_login.feature");
formatter.feature({
  "name": "login",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can successfully login",
  "keyword": "Then "
});
formatter.match({
  "location": "login.user_can_successfully_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC.Log.002.002 - user can successfully login with email address capital letters",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials with email address capital letters and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_with_email_address_capital_letters_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can successfully login",
  "keyword": "Then "
});
formatter.match({
  "location": "login.user_can_successfully_login()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/03_profile.feature");
formatter.feature({
  "name": "profile",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC.Upd.Prof.001.001 - user can update profile",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "profile.user_is_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button profile",
  "keyword": "When "
});
formatter.match({
  "location": "profile.user_click_button_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click navbar user profile",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_navbar_user_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user update profile",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_update_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click simpan button",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully update profile",
  "keyword": "Then "
});
formatter.match({
  "location": "profile.user_successfully_update_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "user can not update profile",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is already on homepage",
  "keyword": "Given "
});
formatter.step({
  "name": "user click button profile",
  "keyword": "When "
});
formatter.step({
  "name": "user click navbar user profile",
  "keyword": "And "
});
formatter.step({
  "name": "user update profile but \u003ccondition\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "user click simpan button",
  "keyword": "And "
});
formatter.step({
  "name": "user get pop-up \u003cresult\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "case_id",
        "condition",
        "result"
      ]
    },
    {
      "cells": [
        "TC.Upd.Prof.001.002",
        "leave nama field empty",
        "please fill out this field"
      ]
    },
    {
      "cells": [
        "TC.Upd.Prof.001.003",
        "without select kota",
        "please select an item in the list"
      ]
    },
    {
      "cells": [
        "TC.Upd.Prof.001.004",
        "leave alamat field empty",
        "please fill out this field"
      ]
    },
    {
      "cells": [
        "TC.Upd.Prof.001.005",
        "leave no handphone field empty",
        "please fill out this field"
      ]
    }
  ]
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user can not update profile",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "profile.user_is_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button profile",
  "keyword": "When "
});
formatter.match({
  "location": "profile.user_click_button_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click navbar user profile",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_navbar_user_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user update profile but leave nama field empty",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_update_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click simpan button",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user get pop-up please fill out this field",
  "keyword": "Then "
});
formatter.match({
  "location": "profile.user_get_pop_up(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user can not update profile",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "profile.user_is_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button profile",
  "keyword": "When "
});
formatter.match({
  "location": "profile.user_click_button_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click navbar user profile",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_navbar_user_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user update profile but without select kota",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_update_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click simpan button",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user get pop-up please select an item in the list",
  "keyword": "Then "
});
formatter.match({
  "location": "profile.user_get_pop_up(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user can not update profile",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "profile.user_is_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button profile",
  "keyword": "When "
});
formatter.match({
  "location": "profile.user_click_button_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click navbar user profile",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_navbar_user_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user update profile but leave alamat field empty",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_update_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click simpan button",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user get pop-up please fill out this field",
  "keyword": "Then "
});
formatter.match({
  "location": "profile.user_get_pop_up(String)"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user can not update profile",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user is already on homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "profile.user_is_already_on_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button profile",
  "keyword": "When "
});
formatter.match({
  "location": "profile.user_click_button_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click navbar user profile",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_navbar_user_profile()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user update profile but leave no handphone field empty",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_update_profile(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click simpan button",
  "keyword": "And "
});
formatter.match({
  "location": "profile.user_click_simpan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user get pop-up please fill out this field",
  "keyword": "Then "
});
formatter.match({
  "location": "profile.user_get_pop_up(String)"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/04_search_product.feature");
formatter.feature({
  "name": "Search Product Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "TC-Sch-001 User want searches for available items in the system",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user login with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_login_with_valid_credentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user already to dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "notification_menu.user_already_to_dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user inputs the keyword buku in the search field",
  "keyword": "When "
});
formatter.match({
  "location": "search_product.user_inputs_the_keyword_buku_in_the_search_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click enter on the keyboard",
  "keyword": "And "
});
formatter.match({
  "location": "search_product.user_click_enter_on_the_keyboard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "system will display a list of products with name buku",
  "keyword": "Then "
});
formatter.match({
  "location": "search_product.system_will_display_a_list_of_products_with_name_buku()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC-Sch-002 User wants to find product based on category",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user login with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_login_with_valid_credentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user already to dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "notification_menu.user_already_to_dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click category name hobi",
  "keyword": "When "
});
formatter.match({
  "location": "search_product.user_click_category_name_hobi()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the system will display a list of products with categories hobi",
  "keyword": "Then "
});
formatter.match({
  "location": "search_product.the_system_will_display_a_list_of_products_with_categories_hobi()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC-Sch-003 user wants to search for items by not log in",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user already to dashboard page as a guest",
  "keyword": "Given "
});
formatter.match({
  "location": "search_product.user_already_to_dashboard_page_as_a_guest()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user inputs the keyword buku in the search field",
  "keyword": "When "
});
formatter.match({
  "location": "search_product.user_inputs_the_keyword_buku_in_the_search_field()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click enter on the keyboard",
  "keyword": "And "
});
formatter.match({
  "location": "search_product.user_click_enter_on_the_keyboard()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "system will display a list of products with name buku",
  "keyword": "Then "
});
formatter.match({
  "location": "search_product.system_will_display_a_list_of_products_with_name_buku()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/05_notification_menu.feature");
formatter.feature({
  "name": "notification menu",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Notification"
    }
  ]
});
formatter.scenario({
  "name": "User wants to access the notification menu",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Notification"
    }
  ]
});
formatter.step({
  "name": "user login with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_login_with_valid_credentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user already to dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "notification_menu.user_already_to_dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click button notifikasi menu",
  "keyword": "When "
});
formatter.match({
  "location": "notification_menu.user_click_button_notifikasi_menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "system will display a list of notifications received by the user",
  "keyword": "Then "
});
formatter.match({
  "location": "notification_menu.system_will_display_a_list_of_notification_received_by_the_user()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/06_add_new_product.feature");
formatter.feature({
  "name": "add new product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "user is not succesfully add new product data because not fill \u003ccondition\u003e field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.step({
  "name": "user click jual button",
  "keyword": "And "
});
formatter.step({
  "name": "user redirect to add product page",
  "keyword": "And "
});
formatter.step({
  "name": "user is input the product data except \u003ccondition\u003e field",
  "keyword": "When "
});
formatter.step({
  "name": "user click terbitkan button",
  "keyword": "And "
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "case_id",
        "condition"
      ]
    },
    {
      "cells": [
        "TCW.Prod.001.002",
        "Name"
      ]
    },
    {
      "cells": [
        "TCW.Prod.001.003",
        "Price"
      ]
    },
    {
      "cells": [
        "TCW.Prod.001.004",
        "Category"
      ]
    },
    {
      "cells": [
        "TCW.Prod.001.005",
        "Description"
      ]
    }
  ]
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully add new product data because not fill Name field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click jual button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_jual_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to add product page",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_redirect_to_add_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is input the product data except Name field",
  "keyword": "When "
});
formatter.match({
  "location": "add_new_product.user_is_input_product_data_except_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully add new product data because not fill Price field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click jual button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_jual_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to add product page",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_redirect_to_add_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is input the product data except Price field",
  "keyword": "When "
});
formatter.match({
  "location": "add_new_product.user_is_input_product_data_except_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully add new product data because not fill Category field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click jual button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_jual_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to add product page",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_redirect_to_add_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is input the product data except Category field",
  "keyword": "When "
});
formatter.match({
  "location": "add_new_product.user_is_input_product_data_except_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully add new product data because not fill Description field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click jual button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_jual_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to add product page",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_redirect_to_add_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is input the product data except Description field",
  "keyword": "When "
});
formatter.match({
  "location": "add_new_product.user_is_input_product_data_except_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TCW.Prod.001.001 user is succesfully add new product data",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click jual button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_jual_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to add product page",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_redirect_to_add_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is input all the product data fields",
  "keyword": "When "
});
formatter.match({
  "location": "add_new_product.user_is_input_product_data()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_click_terbitkan_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully add new product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_successfully_add_new_product()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/07_edit_product.feature");
formatter.feature({
  "name": "add product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "user is not succesfully edit product data because delete \u003ccondition\u003e field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.step({
  "name": "user click daftar jual saya button",
  "keyword": "And "
});
formatter.step({
  "name": "user is choosing the product to edit",
  "keyword": "And "
});
formatter.step({
  "name": "user redirect to product detail page and click edit",
  "keyword": "And "
});
formatter.step({
  "name": "user is edit the product data but clear \u003ccondition\u003e field",
  "keyword": "When "
});
formatter.step({
  "name": "user click terbitkan button after edit",
  "keyword": "And "
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "case_id",
        "condition"
      ]
    },
    {
      "cells": [
        "TCW.Edit.001.002",
        "Name"
      ]
    },
    {
      "cells": [
        "TCW.Edit.001.003",
        "Price"
      ]
    },
    {
      "cells": [
        "TCW.Edit.001.004",
        "Category"
      ]
    },
    {
      "cells": [
        "TCW.Edit.001.005",
        "Description"
      ]
    }
  ]
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully edit product data because delete Name field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click daftar jual saya button",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_daftar_jual_saya_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is choosing the product to edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_is_choosing_the_product_to_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to product detail page and click edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_redirect_to_product_detail_page_and_click_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is edit the product data but clear Name field",
  "keyword": "When "
});
formatter.match({
  "location": "edit_product.user_is_edit_the_product_data_but_clear_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button after edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_terbitkan_button_after_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully edit product data because delete Price field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click daftar jual saya button",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_daftar_jual_saya_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is choosing the product to edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_is_choosing_the_product_to_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to product detail page and click edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_redirect_to_product_detail_page_and_click_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is edit the product data but clear Price field",
  "keyword": "When "
});
formatter.match({
  "location": "edit_product.user_is_edit_the_product_data_but_clear_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button after edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_terbitkan_button_after_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully edit product data because delete Category field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click daftar jual saya button",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_daftar_jual_saya_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is choosing the product to edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_is_choosing_the_product_to_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to product detail page and click edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_redirect_to_product_detail_page_and_click_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is edit the product data but clear Category field",
  "keyword": "When "
});
formatter.match({
  "location": "edit_product.user_is_edit_the_product_data_but_clear_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button after edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_terbitkan_button_after_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user is not succesfully edit product data because delete Description field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click daftar jual saya button",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_daftar_jual_saya_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is choosing the product to edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_is_choosing_the_product_to_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to product detail page and click edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_redirect_to_product_detail_page_and_click_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is edit the product data but clear Description field",
  "keyword": "When "
});
formatter.match({
  "location": "edit_product.user_is_edit_the_product_data_but_clear_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button after edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_terbitkan_button_after_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user will not sucessfully add the product",
  "keyword": "Then "
});
formatter.match({
  "location": "add_new_product.user_will_not_sucessfully_add_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TCW.Edit.001.001 user is succesfully edit product data",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user in homepage",
  "keyword": "And "
});
formatter.match({
  "location": "add_new_product.user_in_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click daftar jual saya button",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_daftar_jual_saya_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is choosing the product to edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_is_choosing_the_product_to_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user redirect to product detail page and click edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_redirect_to_product_detail_page_and_click_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is edit all the product data fields",
  "keyword": "When "
});
formatter.match({
  "location": "edit_product.user_is_edit_all_the_product_data_fields()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click terbitkan button after edit",
  "keyword": "And "
});
formatter.match({
  "location": "edit_product.user_click_terbitkan_button_after_edit()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully edit the product",
  "keyword": "Then "
});
formatter.match({
  "location": "edit_product.user_successfully_edit_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/08_buy_product.feature");
formatter.feature({
  "name": "buy product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "user want to buy product after login",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user fill the login page with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "buy_product.user_fill_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully login",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_successfully_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose the product that want to buy",
  "keyword": "When "
});
formatter.match({
  "location": "buy_product.user_choose_the_product_that_want_to_buy()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input the nominal of product offer and click submit",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_input_the_nominal()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully offer the product",
  "keyword": "Then "
});
formatter.match({
  "location": "buy_product.user_successfully_offer()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user want to buy product before login",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user go to homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "buy_product.user_go_to_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose the product",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_choose_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input product offer and click submit",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_input_product_offer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can see alert message that user must login first",
  "keyword": "Then "
});
formatter.match({
  "location": "buy_product.user_can_see_alert_message()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user want to buy product with 0 on the offer page",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user already login",
  "keyword": "Given "
});
formatter.match({
  "location": "buy_product.user_already_login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose the product to buy",
  "keyword": "When "
});
formatter.match({
  "location": "buy_product.user_choose_the_product_to_buy()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input the nominal of product offer with 0 and click submit",
  "keyword": "And "
});
formatter.match({
  "location": "buy_product.user_input_the_nominal_of_product_offer_with_0()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully buy the product",
  "keyword": "Then "
});
formatter.match({
  "location": "buy_product.user_stay_in_the_product_page()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/09_sell_product.feature");
formatter.feature({
  "name": "sell product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "user want to sell product after login",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user login with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_login_with_valid_credentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user view the notification",
  "keyword": "When "
});
formatter.match({
  "location": "sell_product.user_view_the_notification()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user choose product offer",
  "keyword": "And "
});
formatter.match({
  "location": "sell_product.user_choose_product_offer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user accept the offer from buyer",
  "keyword": "And "
});
formatter.match({
  "location": "sell_product.user_accept_the_offer()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully sell the product",
  "keyword": "Then "
});
formatter.match({
  "location": "sell_product.user_stay_on_the_offer_page()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "user want to sell product before login",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user go to home page",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_go_to_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click the sell button",
  "keyword": "When "
});
formatter.match({
  "location": "sell_product.user_click_the_sell_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can see pop up alert message that user must login first",
  "keyword": "Then "
});
formatter.match({
  "location": "sell_product.user_can_see_pop_up_alert()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/10_delete_product.feature");
formatter.feature({
  "name": "Delete Product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "TC-Delete.Product-001 User wants to delete product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user login with valid credentials",
  "keyword": "Given "
});
formatter.match({
  "location": "sell_product.user_login_with_valid_credentials()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user already to dashboard page",
  "keyword": "And "
});
formatter.match({
  "location": "notification_menu.user_already_to_dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click menu daftar jual saya",
  "keyword": "When "
});
formatter.match({
  "location": "delete_product.user_click_menu_daftar_jual_saya()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click detail product",
  "keyword": "And "
});
formatter.match({
  "location": "delete_product.user_click_detail_product()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click delete",
  "keyword": "And "
});
formatter.match({
  "location": "delete_product.user_click_delete()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the system will delete the product",
  "keyword": "Then "
});
formatter.match({
  "location": "delete_product.the_system_will_delete_the_product()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("Include/features/11_logout.feature");
formatter.feature({
  "name": "Logout",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "TC.Log.002.001 - user can successfully login with valid credentials",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "user is in login page",
  "keyword": "Given "
});
formatter.match({
  "location": "login.user_is_in_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user input valid credentials and click login button",
  "keyword": "When "
});
formatter.match({
  "location": "login.user_input_valid_credentials_and_click_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user can successfully login",
  "keyword": "And "
});
formatter.match({
  "location": "login.user_can_successfully_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "TC.Log.002.001 - user want to log out",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user click button profile in header",
  "keyword": "And "
});
formatter.match({
  "location": "logout.user_click_button_profile_in_header()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click keluar",
  "keyword": "And "
});
formatter.match({
  "location": "logout.user_click_keluar()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user successfully log out",
  "keyword": "Then "
});
formatter.match({
  "location": "logout.user_successfully_log_out()"
});
formatter.result({
  "status": "passed"
});
});